package com.som_itsolutions.customspinner;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by som on 23/11/15.
 */
public class CustomSpinnerAdapter extends ArrayAdapter<SpinnerDataModel> {

    private Context mContext;
    private List<SpinnerDataModel> mDataList;

    public CustomSpinnerAdapter(Context context, int resource, List<SpinnerDataModel> objects) {
        super(context, R.layout.spinnerrow_layout, objects);

        this.mContext = context;
        mDataList = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.spinnerrow_layout, parent, false);

        String rowText = mDataList.get(position).getSpinnerText();
        int  imageId = mDataList.get(position).getImageId();

        ImageView imageV = (ImageView)row.findViewById(R.id.imageView);
        TextView tv = (TextView)row.findViewById(R.id.textView);

        Bitmap bm = BitmapFactory.decodeResource(MainActivity.getMainAppContext().getResources(), imageId);
        imageV.setImageBitmap(bm);
        tv.setText(rowText);

        return row;
    }
}
