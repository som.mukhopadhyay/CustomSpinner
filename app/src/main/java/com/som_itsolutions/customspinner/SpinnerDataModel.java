package com.som_itsolutions.customspinner;

/**
 * Created by som on 23/11/15.
 */
public class SpinnerDataModel {

    private String mSpinnerText;
    private int mImageId;

    public SpinnerDataModel(){

    }

    public SpinnerDataModel(String text, int id){
        this.mSpinnerText = text;
        this.mImageId = id;
    }

    //getter functions
    public String getSpinnerText(){
        return mSpinnerText;
    }

    public int getImageId(){
        return mImageId;
    }

    //setter functions
    public void setSpinnnerText(String text){
        this.mSpinnerText = text;
    }

    public void setImageUri(int id){
        this.mImageId = id;
    }
}
